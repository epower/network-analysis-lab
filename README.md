# network-analysis-lab

The following is a playbook for network-analysis-lab. 

## How to Use

This is an [Ansible](https://github.com/ansible/ansible) playbook. I've found the best results come from running on a Linux/OSX host.

### Requirements

- A target VM with OpenSSH Server installed and running.
- A host with Python and [Pip](https://pypi.python.org/pypi/pip) installed.

### Method

Fork/clone this repository.

In the repository directory:

    $ pip install -r requirements.txt

Edit the inventory; adding the hostname or IP address of the target VM.
Edit 'ansible.cfg' making sure you're happy with the remote user and authentication method (password, key, etc.).

    $ pynt

This runs the playbook with the inventory and the ansible configuration.

### Results


## Contribution

- Post an issue to the [issue tracker](http://gitlab.tssg.org/) 
- Fork the repo, make some changes and submit a [Merge Request](http://gitlab.tssg.org/)
- Patches, docs, tutorials and feedback are all welcome.

Eamonn Power epower@tssg.org

