import sys
import subprocess
from pynt import task

@task()
def run_playbook():
    print "running the playbook"
    subprocess.call(["ansible-playbook", "--connection=local", "-i", "inventory", "site.yml", "-K"]) 

@task()
def test():
    print "testing the playbook"
    subprocess.call(["ansible-playbook", "--connection=local", "-i", "inventory", "site.yml", "--check"]) 

@task()
def get_roles():
    print "fetching the roles from Ansible Galaxy"
    subprocess.call(["ansible-galaxy", "install", "-r", "roles.yml", "-p", "roles", "--force"])

@task()
def ping():
    print "pinging..."
    subprocess.call(["ansible", "-i", "inventory", "-m", "ping", "all"])

__DEFAULT__ = run_playbook
